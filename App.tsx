// import { } from 'react-native'
import React from 'react';
import { LogBox, StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RootNavigation } from './src/navigation';
import { ApolloProvider } from '@apollo/client';
import { client } from './src/services/api';
import FlashMessage from "react-native-flash-message";

LogBox.ignoreAllLogs();
const App = () => {

	return (
		<ApolloProvider client={client}>
			<SafeAreaProvider>
				<StatusBar
					barStyle={'dark-content'}
					translucent
					backgroundColor={'transparent'}
				/>
				<RootNavigation />
				<FlashMessage position="top" />
			</SafeAreaProvider>
		</ApolloProvider>
	);
};

export default App;
