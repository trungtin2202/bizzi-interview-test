# HOW TO RUN PROJECT

first, you need install node_modules, project friendly with both yarn and npm


```bash
yarn install 
or
npm install
```

Continues to install Cocoapods, if you have `Pods` folder and `Podfile.lock`, please remove and reinstall with command: 
```bash
yarn ios:clean 
or
npm ios:clean
```

For android, you only need run: 

```bash
yarn android
``` 

-------------------

# List of Features
- Sign up
- Sign in
- Forget password (send link to email for create new password)
- CRUD photo (GraphQL)
- Handle error, success
- Using react-hook-form for best performance
-----
# Estimated time vs actual working time:
- Learn graphQL(4 - 3h)
- Create architect (2 - 2h)
- Build layout CRUD (6 - 6h)
- Call graphQL and bind dates to layout (8 - 6h)
- Create and upload repo to git (1 - 30m)
- Bugs fix (0 - 3h)
-----
# Screenshot
Link: https://drive.google.com/drive/folders/1mMhKq6jf4T85BIMmcc1mu8blKSkNcm77?usp=sharing

----
Create by Johnny Wed 17 Aug 2022
