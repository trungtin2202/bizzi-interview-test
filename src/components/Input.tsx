import { StyleSheet, TextInput, TextInputProps, } from 'react-native'
import React from 'react'

import { colors, normalize, normalizeHorizontal, } from '../helper';
const InputComp = (props: TextInputProps, ref: any) => {
    const { style: customStyle } = props
    return (
        <TextInput
            {...props}
            ref={ref}
            style={[styles.container, customStyle]}
        />
    )
}
const InputForwardRef = React.forwardRef(InputComp);
export const Input = React.memo(InputForwardRef);

const styles = StyleSheet.create({
    container: {
        width: '90%',
        height: normalize(40),
        borderWidth: 1,
        borderColor: colors.PRIMARY,
        alignSelf: 'center',
        marginVertical: normalizeHorizontal(10),
        paddingLeft: normalizeHorizontal(10),
        color: colors.GRAY
    },
})