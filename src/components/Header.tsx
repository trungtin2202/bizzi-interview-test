import { StyleSheet, Text, TouchableOpacity, View, ViewProps } from 'react-native'
import React from 'react'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { colors, normalize, normalizeHorizontal } from '../helper';
import FastImage from 'react-native-fast-image';
import { IC_ADD, IC_BACK } from '../assets';
import NavigationService from '../navigation/navigationServices';

interface IHeader {
    isShowBackButton?: boolean,
    title: string,
    onPress?: () => void,
    rightIcon?: string, 
    onRightPress?: () => void,
    isRight?: boolean
}
const SUFFIX_FLEX = 0.2

const HeaderComp = (props: IHeader & ViewProps) => {
    const inset = useSafeAreaInsets();
    const { 
        isShowBackButton = false, 
        title = "", 
        onPress = () => NavigationService.goBack(),
        isRight = false,
        rightIcon = IC_ADD,
        onRightPress = () => {}
    } = props;

    return (
        <View style={[styles.container, { paddingTop: inset.top + normalize(8) }]}>
            {isShowBackButton ?
                <TouchableOpacity onPress={onPress} style={styles.btnBack}>
                    <FastImage
                        source={IC_BACK}
                        style={styles.icBack}
                        tintColor={colors.WHITE}
                    />
                </TouchableOpacity> :
                <View style={{ flex: SUFFIX_FLEX }} />
            }
            <View style={{ flex: 0.6 }}>
                <Text style={styles.txtHeader}>{title}</Text>
            </View>
            {isRight ?
                <TouchableOpacity onPress={onRightPress} style={styles.btnBack}>
                    <FastImage
                        source={rightIcon}
                        style={styles.icBack}
                        tintColor={colors.WHITE}
                    />
                </TouchableOpacity> :
                <View style={{ flex: SUFFIX_FLEX }} />
            }
        </View>
    )
}

export const Header = React.memo(HeaderComp)

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.PRIMARY,
        paddingBottom: normalize(12),
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    txtHeader: {
        fontSize: normalize(18),
        color: colors.WHITE,
        fontWeight: '700',
        textAlign: 'center'
    },
    btnBack: {
        flex: SUFFIX_FLEX,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icBack: {
        width: normalizeHorizontal(24),
        aspectRatio: 1,
    }
})