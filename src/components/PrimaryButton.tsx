import { StyleSheet, Text, TouchableOpacity, View, TouchableOpacityProps, ViewStyle, TextStyle, ActivityIndicator } from 'react-native'
import React from 'react'
import { colors, FONT_SIZE_18, normalize } from '../helper';
interface ButtonProps {
    contentContainerStyle?: ViewStyle,
    textStyle?: TextStyle,
    title: String,
    doPress?: (event: any) => Promise<void>,
    isLoading?: boolean
}
const PrimaryButtonComp = (props: ButtonProps & TouchableOpacityProps) => {
    const { contentContainerStyle = {}, textStyle, title = "Login", doPress = (event: any) => { }, isLoading } = props;
    return (
        <TouchableOpacity {...props} onPress={doPress} style={[styles.container, contentContainerStyle]}>
            {isLoading ? <ActivityIndicator /> : <Text style={[styles.title, textStyle]}>{title}</Text>}
        </TouchableOpacity>
    )
}

export const PrimaryButton = React.memo(PrimaryButtonComp)

const styles = StyleSheet.create({
    container: {
        marginTop: normalize(24),
        backgroundColor: colors.PRIMARY,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        height: normalize(60)
    },
    title: {
        fontSize: FONT_SIZE_18,
        color: 'white'
    }
})