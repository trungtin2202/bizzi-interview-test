import { useState } from 'react'

export const useInput = (init: string = '') => {
    const [value, setValue] = useState(init)

    const reset = () => setValue('')

    const setValueInput = (input: string) => setValue(input)

    const bind = {
        value,
        onChangeText: (text: string) => setValue(text),
    }

    return [value, bind, reset, setValueInput]
}
