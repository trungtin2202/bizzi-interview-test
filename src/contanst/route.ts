export const ROUTE = {
    FIRST: 'introScreen',
    LOGIN: 'loginScreen',
    REGISTER: 'registerScreen',
    HOME: 'homeScreen',
    CREATE_POST: 'createPostScreen',
    PHOTO: "photoScreen"
};