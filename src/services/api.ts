import { ApolloClient, InMemoryCache, } from '@apollo/client';
export const API_END_POINT = 'https://graphqlzero.almansi.me/api';

export const client = new ApolloClient({
  uri: API_END_POINT,
  cache: new InMemoryCache(),
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'Application/json',
  },
});