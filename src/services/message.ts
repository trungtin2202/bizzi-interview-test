import { showMessage } from 'react-native-flash-message';
import { colors } from '../helper';
export const showError = (des: string) => {
    showMessage({
        message: 'Error',
        description: des,
        type: 'danger',
        textStyle: {
            color: colors.WHITE,
        },
    });
};

export const showSuccess = (des: string) => {
    showMessage({
        message: 'Success',
        description: des,
        type: 'success',
        textStyle: {
            color: colors.WHITE,
        },
    });
};
