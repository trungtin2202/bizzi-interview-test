import { gql } from '@apollo/client'

export const CREATE_PHOTO = gql`
  mutation createPhoto ($input: CreatePhotoInput!) {
    createPhoto(input: $input) {
      title
      url
      thumbnailUrl
    }
  }
`;

export const UPDATE_PHOTO_BY_ID = gql`
  mutation updatePhoto ($id: ID!, $input: UpdatePhotoInput!) {
    updatePhoto(id: $id, input: $input) {
      title
      url
      thumbnailUrl
    }
  }
`;

export const DELETE_PHOTO_BY_ID = gql`
  mutation ($id: ID!) {
    deletePhoto(id: $id)
  }
`;

// * syntax muation 
// * Mutation `mutationName` 
// -> args (apollo received) 
// -> model/method in schema 
// -> args to schema
// model what you want 
// for method POST, PUT, DELETE