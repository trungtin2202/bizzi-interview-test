import { gql } from '@apollo/client';
export const GET_PHOTO_ALBUM_SINGLE = gql`
  # // args function
  query getPhotoAlbumSingle ($id: ID!) { 
    # id query
    photo(id: $id) {
      album {
        id
        title
        user {
          id
        }
      }
    }
  }
`;

// *syntax query `functionName` for apollo 
// -> args (GIÁ TRỊ CLIENT TRUYỀN VÀO APOLLO) /// NẰM DƯỚI CLIENT... CHƯA LÊN SV
// -> schema model 
// -> LẤY GIÁ TRỊ TRUYỀN VÀO ĐỂ ĐẨY LÊN SCHEMA // BẮT ĐẦU QUERY
// -> model what you want
// for method GET
export const GET_ALL_PHOTOS_PAGING = gql`
  query getAllPhotosPaging (
  $options: PageQueryOptions
) {
  photos(options: $options) {
    data {
      id
      title
      url
      thumbnailUrl
      album {
        title
        user {
          name
        }
      }
    }
    meta {
      totalCount
    }
  }
}
`;
