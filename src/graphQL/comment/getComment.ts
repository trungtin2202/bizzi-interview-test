import { gql } from '@apollo/client';

export const GET_COMMENT_PAGING = gql`
    query (
        $options: PageQueryOptions!
        ) {
        comments(options: $options) {
            data {
                id
                name
                email
                body
                post {
                    id
                    title
                    body
                }
            }
            meta {
                totalCount
            }
  }
    }
`