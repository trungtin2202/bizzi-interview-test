import { useRef } from 'react';
import { object, string, ref } from 'yup';

export const useRegister = () => {
    const fullnameRef = useRef<any>(undefined)
    const passwordRef = useRef<any>(undefined);
    const confirmPasswordRef = useRef<any>(undefined)

    const focusPassword = () => {
        passwordRef.current && passwordRef.current?.focus();
    };
    const focusConfirmPassword = () => {
        confirmPasswordRef.current && confirmPasswordRef.current?.focus();
    };

    const validationSchema = object().shape({
        email: string()
            .trim()
            .required('Email is required'),
        password: string()
            .required('Password is required'),
        confirmPassword: string()
            .required('Confirm password is required')
            .oneOf([ref('password'), null], 'Confirm password is not match'),
    });

    return {
        focusPassword,
        focusConfirmPassword,
        passwordRef,
        validationSchema,
        fullnameRef,
        confirmPasswordRef
    };
};
