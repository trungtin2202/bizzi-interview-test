import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { Input, PrimaryButton } from '../../components'
import { IMG_LOGO, IC_PASS } from '../../assets'
import FastImage from 'react-native-fast-image'
import { colors, FONT_SIZE_16, FONT_SIZE_18, FONT_SIZE_20, normalize, normalizeHorizontal } from '../../helper'
import auth from '@react-native-firebase/auth';
import NavigationService from '../../navigation/navigationServices'
import { ROUTE } from '../../contanst'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRegister } from './hook/useRegister'
import { Controller, useForm } from 'react-hook-form'
import { showError, showSuccess } from '../../services/message'
interface initialValuesTypes {
	email: string,
	password: string,
	confirmPassword: string
}
export const Register = () => {

	const initialValues = {
		email: '',
		password: '',
		confirmPassword: '',
	};

	const [isLoading, setIsLoading] = useState<boolean>(false)
	const {
		focusPassword,
		focusConfirmPassword,
		passwordRef,
		validationSchema,
		confirmPasswordRef } = useRegister();
	const { control, handleSubmit, formState: { errors }, setValue } = useForm({
		defaultValues: initialValues,
		resolver: yupResolver(validationSchema)
	});
	const onRegister = async (data: any) => {
		const {
			email,
			password,
		}: initialValuesTypes = data
		setIsLoading(true)
		const response = await auth()
			.createUserWithEmailAndPassword(email, password)
			.catch(error => {
				showError(error.message)
			});
		if (response) {
			showSuccess('Create user success')
			setValue('email', '')
			setValue('confirmPassword', '')
			setValue('password', '')
			NavigationService.navigate(ROUTE.LOGIN, { email: email })
		}
		setIsLoading(false)
	}
	return (
		<View style={styles.container}>
			<View style={{ alignItems: 'center' }}>
				<FastImage
					source={IMG_LOGO}
					style={styles.imgLogo}
					resizeMode={'contain'}
				/>
				<Text style={styles.header}>{'Register'}</Text>
				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, value } }) => (
						<Input
							value={value}
							onChangeText={onChange}
							placeholder='Email'
							autoCapitalize='none'
							onSubmitEditing={focusPassword}
						/>
					)}
					name="email"
				/>

				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, value } }) => (
						<Input
							ref={passwordRef}
							value={value}
							onChangeText={onChange}
							placeholder='Password'
							secureTextEntry={true}
							onSubmitEditing={focusConfirmPassword}
						/>
					)}
					name="password"
				/>

				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, value } }) => (
						<Input
							ref={confirmPasswordRef}
							value={value}
							onChangeText={onChange}
							placeholder='confirmPassword'
							secureTextEntry={true}
							onSubmitEditing={handleSubmit(onRegister)}
						/>
					)}
					name="confirmPassword"
				/>
			</View>
			<PrimaryButton
				doPress={onRegister}
				contentContainerStyle={{ alignSelf: 'center' }}
				title={'Register'.toUpperCase()}
				isLoading={isLoading}
			/>
			<View style={styles.footer}>
				<Text style={{ fontSize: FONT_SIZE_18 }}>{'Already have an account? '}
					<TouchableOpacity onPress={() => NavigationService.goBack()} style={{ justifyContent: 'flex-end', marginBottom: -2 }}>
						<Text style={styles.txtFooterHighlight}>{'Login now!'}</Text>
					</TouchableOpacity></Text>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	imgLogo: {
		alignSelf: 'center',
		width: '100%',
		height: normalize(280),
	},
	container: {
		flex: 1,
	},
	btnForgetPass: {
		marginTop: normalize(12),
		marginRight: normalizeHorizontal(20)
	},
	txtForgetPass: {
		textAlign: 'right',
		fontSize: FONT_SIZE_16,
		color: colors.GRAY_DARK
	},
	header: {
		fontSize: FONT_SIZE_20,
		marginTop: normalize(24),
		color: colors.PRIMARY
	},
	footer: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		marginBottom: normalize(48)
	},
	txtFooterHighlight: {
		fontSize: FONT_SIZE_16,
		color: colors.PRIMARY
	}
})