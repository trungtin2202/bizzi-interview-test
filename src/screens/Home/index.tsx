import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import { useLazyQuery, useMutation } from '@apollo/client';
import { DELETE_PHOTO_BY_ID, GET_ALL_PHOTOS_PAGING } from '../../graphQL';
import { colors, normalize, shadow } from '../../helper';
import FastImage from 'react-native-fast-image';
import { Header } from '../../components';
import NavigationService from '../../navigation/navigationServices';
import { ROUTE } from '../../contanst';
import { useNavigation } from '@react-navigation/core';
import { IC_DELETE } from '../../assets';
import { showError, showSuccess } from '../../services/message';

export interface IItem {
    id: string,
    thumbnailUrl: string,
    title: string,
    url: string,
    __typename: string,
    album: {
        title: string,
        user: {
            __typename: string,
            name: string,
        }
    }
}

interface IListItem {
    item: IItem,
    index: Number
}

export const Home = () => {
    const navi = useNavigation<any>()
    const [page, setPage] = useState<number>(1);
    const [isRefresh, setIsRefresh] = useState<boolean>(false)
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [isLoadmore, setIsLoadmore] = useState<boolean>(false)
    const paging = {
        options: {
            paginate: {
                page: page,
                limit: 5
            }
        }
    }
    const [getAllPhotosPaging, { error, data, refetch }] = useLazyQuery(GET_ALL_PHOTOS_PAGING, {
        variables: paging
    });

    const [deletePhoto] = useMutation(DELETE_PHOTO_BY_ID);
    const [dataList, setDataList] = useState<IItem[]>([]);
    useEffect(() => {
        setIsLoading(true)
        //hot reload will get error. 
        const sub = navi.addListener('focus', () => {
            getAllPhotosPaging().then(() => {
                setIsLoading(false)
            });
        })
        return sub
    }, [])
    /**
     * check value and set to list
     * loadmore and getlist function
     * @params data
     */
    useEffect(() => {
        if (data?.photos?.data?.length > 0 && !isRefresh) {
            const dataTemp = [...dataList];
            const newData = dataTemp.concat(data?.photos?.data)
            setDataList(newData)
        }
    }, [data])

    const onRefresh = async () => {
        setIsRefresh(true)
        const pagingRefresh = {
            options: {
                paginate: {
                    page: 1,
                    limit: 5
                }
            }
        }
        const response = await refetch(pagingRefresh);
        setDataList(response?.data?.photos?.data)
        setIsRefresh(false)
        setPage(1)
    }

    const onLoadmore = async () => {
        setIsLoadmore(true)
        await refetch(paging)
        setPage(page + 1)
        setIsLoadmore(false)
    }
    const onDelete = async (id: string) => {
        const dataTemp = [...dataList]
        const response = await deletePhoto({ variables: { id: parseInt(id) } })
        if (response?.data?.deletePhoto) {
            const index = dataTemp.findIndex(item => item.id == id);
            if (index > -1) {
                dataTemp.splice(index, 1)
                setDataList(dataTemp)
                showSuccess('Delete item success')
            }else {
                onRefresh()
                showSuccess('Delete item success')
            }
        } else {
            showError('Something went wrong')
        }
    }

    const renderItem = useCallback(({ item, index }: IListItem) => {
        return (
            <TouchableOpacity style={styles.item} onPress={() => onPressItem(item)}>
                <FastImage
                    source={{ uri: item?.url }}
                    style={styles.imgItem}
                />
                <View style={styles.contentItem}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.txtItemTitle}>{item?.title}</Text>
                        <TouchableOpacity onPress={() => onDelete(item?.id)}>
                            <FastImage
                                source={IC_DELETE}
                                style={{
                                    height: normalize(20),
                                    aspectRatio: 1
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.txtContentItem}>{item?.album?.title}</Text>
                    <Text style={styles.txtNameItem}>{item?.album?.user?.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }, [])

    const onPressItem = (item: IItem) => {
        NavigationService.navigate(ROUTE.PHOTO, item)
    }
    const loadmoreView = () => {
        let render;
        if (isLoadmore) {
            render = <ActivityIndicator size={'large'} style={{ marginTop: normalize(12) }} />
        } else {
            render = <View />
        }
        return render
    }
    return (
        <View style={{ flex: 1, }}>
            <Header
                title={'HOME'}
                isRight
                onRightPress = {() => NavigationService.navigate(ROUTE.PHOTO)}
            />
            <View style={styles.content}>
                <Text style={styles.txtHeaderContent}>{'Photo albums'}</Text>
                {isLoading ? <ActivityIndicator size={'large'} color={'black'} /> : <FlatList
                    data={dataList}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => `${item?.id} ${index}`}
                    refreshing={isRefresh}
                    onRefresh={onRefresh}
                    onEndReachedThreshold={0.1}
                    onEndReached={onLoadmore}
                    contentContainerStyle={{
                        paddingBottom: normalize(120)
                    }}
                    ListFooterComponent={loadmoreView}
                    showsVerticalScrollIndicator={false}
                />}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.PRIMARY,
        paddingBottom: normalize(12),
        alignItems: 'center',
    },
    content: {
        flex: 1,
    },
    txtHeader: {
        fontSize: normalize(18),
        color: colors.WHITE,
        fontWeight: '700'
    },
    txtHeaderContent: {
        fontSize: normalize(16),
        margin: normalize(12)
    },
    item: {
        backgroundColor: colors.WHITE,
        marginVertical: normalize(8),
        flexDirection: 'row',
        width: '90%',
        alignSelf: 'center',
        padding: normalize(12),
        borderRadius: 8,
        ...shadow,
    },
    imgItem: {
        height: normalize(80),
        aspectRatio: 1,
        borderRadius: 4
    },
    contentItem: {
        width: '70%',
        marginLeft: normalize(12),
        justifyContent: 'space-between'
    },
    txtItemTitle: {
        fontSize: normalize(16),
        fontWeight: '500',
        width: '90%'
    },
    txtContentItem: { marginTop: normalize(8), fontSize: normalize(14), fontWeight: '300' },
    txtNameItem: { marginTop: normalize(4), fontSize: normalize(14), fontWeight: '300' }
})