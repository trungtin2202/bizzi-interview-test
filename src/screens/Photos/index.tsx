import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native'
import React, { useEffect, useState } from 'react'
import {
    colors,
    normalize,
    normalizeHorizontal,
} from '../../helper';
import { Header, Input, PrimaryButton } from '../../components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useRoute } from '@react-navigation/core';
import FastImage from 'react-native-fast-image';
import { IMG_LOGO } from '../../assets';
import { usePhoto } from './hook/usePhoto';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'
import { useMutation } from '@apollo/client';
import { CREATE_PHOTO, UPDATE_PHOTO_BY_ID } from '../../graphQL';
import NavigationService from '../../navigation/navigationServices';
import { IItem } from '..';
import { showError } from '../../services/message';
interface IParams {
    title: string
    url: string
    thumbnailUrl: string
}

const initialValues = {
    title: '',
    url: '',
    thumbnailUrl: '',
};

export const Photo = () => {
    const params = useRoute<any>().params;
    const {
        validationSchema,
        focusUrl,
        focusThumbnail,
        titleRef,
        urlRef,
        thumbnailUrlRef
    } = usePhoto();
    const [createPhoto, { loading: createLoading }] = useMutation(CREATE_PHOTO);
    const [updatePhoto, { loading: updateLoading }] = useMutation(UPDATE_PHOTO_BY_ID);
    const { control, handleSubmit, formState: { errors }, setValue } = useForm({
        defaultValues: initialValues,
        resolver: yupResolver(validationSchema)
    });
    useEffect(() => {
        if (params) {
            setValue("thumbnailUrl", params?.thumbnailUrl)
            setValue("url", params?.url)
            setValue("title", params?.title)
        }
    }, [])

    const onUpdate = async (value: IParams) => {
        const updateParams = {
            id: params?.id,
            input: value
        }
        const res = await updatePhoto({ variables: updateParams });
        if (res) {
            setValue("url", "")
            setValue("title", "")
            setValue("thumbnailUrl", "")
            NavigationService.goBack()
        } else {
            showError("something went wrong")
        }
    }

    const onSubmit = async (value: IParams) => {
        const createParams = {
            input: value
        }
        const res = await createPhoto({ variables: createParams });
        if (res) {
            setValue("url", "")
            setValue("title", "")
            setValue("thumbnailUrl", "")
            NavigationService.goBack()
        } else {
            showError("something went wrong")
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <Header
                isShowBackButton
                title={params ? 'Update Photo' : 'Create Photo'}
            />
            <KeyboardAwareScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                enableOnAndroid
                bounces={false}
            >
                <FastImage
                    source={IMG_LOGO}
                    style={styles.logo}
                />
                <View style={styles.containerTip}>
                    <Text style={styles.txtTitle}>{'Title'}</Text>
                    <Controller
                        control={control}
                        rules={{
                            required: true,
                        }}
                        render={({ field: { onChange, value } }) => (
                            <Input
                                ref={titleRef}
                                value={value}
                                onChangeText={onChange}
                                placeholder={'Input your title'}
                                onSubmitEditing={focusUrl}
                                style={styles.tip}
                                multiline
                            />
                        )}
                        name="title"
                    />
                </View>

                <View style={styles.containerTip}>
                    <Text style={styles.txtTitle}>{'URL'}</Text>
                    <Controller
                        control={control}
                        rules={{
                            required: true,
                        }}
                        render={({ field: { onChange, value } }) => (
                            <Input
                                ref={urlRef}
                                value={value}
                                onChangeText={onChange}
                                placeholder={'https://...'}
                                onSubmitEditing={focusThumbnail}
                                style={styles.tip}
                            />
                        )}
                        name="url"
                    />
                </View>

                <View style={styles.containerTip}>
                    <Text style={styles.txtTitle}>{'Thumbnail URL'}</Text>
                    <Controller
                        control={control}
                        rules={{
                            required: true,
                        }}
                        render={({ field: { onChange, value } }) => (
                            <Input
                                ref={thumbnailUrlRef}
                                value={value}
                                onChangeText={onChange}
                                placeholder={'https://...'}
                                onSubmitEditing={params ? handleSubmit(onUpdate) : handleSubmit(onSubmit)}
                                style={styles.tip}
                            />
                        )}
                        name="thumbnailUrl"
                    />
                </View>
            </KeyboardAwareScrollView>
            <View style={{ flex: 1, paddingBottom: normalize(24) }}>
                <PrimaryButton
                    title={params ? 'Update' : 'Create'}
                    doPress={params ? handleSubmit(onUpdate) : handleSubmit(onSubmit)}
                    contentContainerStyle={styles.btnSubmit}
                    isLoading={createLoading || updateLoading}
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.PRIMARY,
        paddingBottom: normalize(12),
        alignItems: 'center',
        marginBottom: normalize(60)
    },
    txtHeader: {
        fontSize: normalize(18),
        color: colors.WHITE,
        fontWeight: '700'
    },
    containerTip: {
        width: '90%',
        height: normalize(100),
        alignSelf: 'center',
        justifyContent: 'center'
    },
    tip: {
        width: '100%',
    },
    txtTitle: {
        fontSize: normalize(12),
        letterSpacing: 0.5,
        fontWeight: '500',
        textTransform: 'uppercase',
        color: colors.BLUE
    },
    btnSubmit: {
        width: '90%',
        height: normalize(50),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.PRIMARY,
        marginTop: normalize(20),
        borderRadius: 40
    },
    txtSubmit: {
        fontSize: normalize(16),
        color: colors.WHITE,
        fontWeight: '600'
    },
    logo: {
        width: normalizeHorizontal(240),
        aspectRatio: 1,
        alignSelf: 'center',
        marginBottom: normalize(24)
    }
})