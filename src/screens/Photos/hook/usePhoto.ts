import { useRef } from 'react';
import { object, string, ref } from 'yup';

export const usePhoto = () => {
    const titleRef = useRef<any>(undefined)
    const urlRef = useRef<any>(undefined);
    const thumbnailUrlRef = useRef<any>(undefined)

    const focusUrl = () => {
        urlRef.current && urlRef.current?.focus();
    };
    const focusThumbnail = () => {
        thumbnailUrlRef.current && thumbnailUrlRef.current?.focus();
    };

    const validationSchema = object().shape({
        title: string()
            .required('title is required'),
        url: string()
            .required('url is required'),
        thumbnailUrl: string()
            .required('thumbnailUrl is required')
    });

    return {
        validationSchema,
        focusUrl,
        focusThumbnail,
        titleRef,
        urlRef,
        thumbnailUrlRef
    };
};
