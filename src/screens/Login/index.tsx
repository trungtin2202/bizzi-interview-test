import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Input, PrimaryButton } from '../../components'
import { IMG_LOGO } from '../../assets'
import FastImage from 'react-native-fast-image'
import { colors, FONT_SIZE_16, FONT_SIZE_18, FONT_SIZE_20, normalize, normalizeHorizontal } from '../../helper'
import { useLogin } from './hook/useLogin'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup';
import auth from '@react-native-firebase/auth';
import NavigationService from '../../navigation/navigationServices'
import { ROUTE } from '../../contanst'
import { showError, showSuccess } from '../../services/message'
import { useRoute } from '@react-navigation/core'

interface initialValuesTypes {
	email: String,
	password: String
}

export const Login = () => {
	const initialValues = {
		email: '',
		password: '',
	}
	const params = useRoute<any>()?.params
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const { focusPassword, passwordRef, validationSchema, emailRef } = useLogin();
	const { control, handleSubmit, formState: { errors }, getValues, setValue } = useForm({
		defaultValues: initialValues,
		resolver: yupResolver(validationSchema)
	});
	const onLogin = async (data: initialValuesTypes) => {
		const { email, password } = data;
		setIsLoading(true)
		if (email != "" && password != "") {
			const authFirebaseTemp = await auth()
				.signInWithEmailAndPassword(email.trim().toLowerCase(), password.trim()).catch((error: any) => {
					showError(error.message)
				})
			if (authFirebaseTemp) {
				NavigationService.reset(ROUTE.HOME)
			}
		}
		setIsLoading(false)
	}

	const navRegister = () => {
		NavigationService.navigate(ROUTE.REGISTER)
	}

	const forgotPassword = () => {
		const email = getValues("email")
		auth().sendPasswordResetEmail(email)
			.then(function (user) {
				showSuccess('Please check your email...')
			}).catch(function (error) {
				showError(error.message)
			})
	}

	useEffect(() => {
		if (params) {
			setValue("email", params?.email)
		}
	}, [])

	return (
		<View style={styles.container}>
			<View style={{ alignItems: 'center' }}>
				<FastImage
					source={IMG_LOGO}
					style={styles.imgLogo}
					resizeMode={'contain'}
				/>
				<Text style={styles.header}>{'Login'}</Text>
				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, value } }) => (
						<Input
							ref={emailRef}
							onChangeText={onChange}
							value={value}
							placeholder='Email'
							autoCapitalize='none'
							onSubmitEditing={focusPassword}
						/>
					)}
					name="email"
				/>

				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, value } }) => (
						<Input
							ref={passwordRef}
							value={value}
							onChangeText={onChange}
							placeholder='Password'
							onSubmitEditing={handleSubmit(onLogin)}
							secureTextEntry={true}
						/>
					)}
					name="password"
				/>
			</View>
			<TouchableOpacity onPress={forgotPassword} style={styles.btnForgetPass}>
				<Text style={styles.txtForgetPass}>{'Forgot password'}</Text>
			</TouchableOpacity>
			<PrimaryButton
				doPress={handleSubmit(onLogin)}
				contentContainerStyle={{ alignSelf: 'center' }}
				title={'Login'}
				isLoading={isLoading}
			/>
			<View style={styles.footer}>
				<Text style={{ fontSize: FONT_SIZE_18 }}>{'Do not have account? '}
					<TouchableOpacity onPress={navRegister} style={{ justifyContent: 'flex-end', marginBottom: -2 }}>
						<Text style={styles.txtFooterHighlight}>{'Register now!'}</Text>
					</TouchableOpacity></Text>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	imgLogo: {
		alignSelf: 'center',
		width: '100%',
		height: normalize(280),
	},
	container: {
		flex: 1,
	},
	btnForgetPass: {
		marginTop: normalize(12),
		marginRight: normalizeHorizontal(20)
	},
	txtForgetPass: {
		textAlign: 'right',
		fontSize: FONT_SIZE_16,
		color: colors.GRAY_DARK
	},
	header: {
		fontSize: FONT_SIZE_20,
		marginTop: normalize(24),
		color: colors.PRIMARY
	},
	footer: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		marginBottom: normalize(48)
	},
	txtFooterHighlight: {
		fontSize: FONT_SIZE_16,
		color: colors.PRIMARY
	}
})