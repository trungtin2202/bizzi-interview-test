import { useRef } from 'react';
import { object, string } from 'yup';

export const useLogin = () => {
    const emailRef = useRef<any>(undefined);
    const passwordRef = useRef<any>(undefined);
    const focusPassword = () => {
        passwordRef.current && passwordRef.current.focus();
    };
    const validationSchema = object().shape({
        email: string().trim().required('Email is required'),
        password: string().required('Password is required'),
    });

    return {
        focusPassword,
        emailRef,
        passwordRef,
        validationSchema,
    };
};
