///ICON
export const IC_EMAIL = require('./icons/ic_email.png')
export const IC_PASS = require('./icons/ic_pass.png')
export const IC_SHOW_PASS = require('./icons/ic_show_pass.png')
export const IC_BACK = require('./icons/ic_back.png')
export const IC_DELETE = require('./icons/ic_delete.png')
export const IC_ADD = require('./icons/ic_add.png')
///IMG
export const IMG_LOGO = require('./images/logo.png')