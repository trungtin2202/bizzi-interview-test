
import { normalize } from './normalize';

//  FONT SIZE
export const FONT_SIZE_12 = normalize(12);
export const FONT_SIZE_14 = normalize(14);
export const FONT_SIZE_15 = normalize(15);
export const FONT_SIZE_16 = normalize(16);
export const FONT_SIZE_18 = normalize(18);
export const FONT_SIZE_20 = normalize(20);

//  Additional font family
export const shadow = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,

  elevation: 4,
};
