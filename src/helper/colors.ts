export const colors = {
    PRIMARY_BACKGROUND: '#FFFFFF',
    TEXT_COLOR: '#131212',
    PRIMARY: '#1317DD',
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    GRAY: '#909193',
    GRAY_DARK: '#5d5e61',
    RED: '#dc4b3b',
    BLUE:'#123678'
}