import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ROUTE } from '../contanst';
import { Home, Photo, Login, Register } from '../screens';
import React, { useEffect } from 'react';
import { navigationRef, isMountedRef } from './navigationServices'
const Stack = createNativeStackNavigator();
export const RootNavigation = () => {
  useEffect(() => {
    isMountedRef.current = true;
    return () => {
      isMountedRef.current = false;
    };
  }, []);

  return (
    <NavigationContainer ref={navigationRef} >
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName={ROUTE.LOGIN}>
        <Stack.Screen name={ROUTE.LOGIN} component={Login} />
        <Stack.Screen name={ROUTE.REGISTER} component={Register} />
        <Stack.Screen name={ROUTE.HOME} component={Home} />
        <Stack.Screen name={ROUTE.PHOTO} component={Photo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
