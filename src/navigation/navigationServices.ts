import React, { useRef } from 'react';

import { StackActions, CommonActions } from '@react-navigation/native';

export const navigationRef = useRef<any>();
export const isMountedRef = useRef<any>();

/**
 * Call this function when you want to navigate to a specific route.
 *
 * @param routeName The name of the route to navigate to. Routes are defined in RootScreen using createStackNavigator()
 * @param params Route parameters.
 */
const navigate = (routeName: string, params?: any) => {
    console.log('LOG_navigate', routeName, params);
    if (isMountedRef.current && navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.navigate(routeName, params);
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
};

const replace = (routeName: string, params?: any) => {
    console.log('LOG_replace', routeName, params);

    if (isMountedRef.current && navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.dispatch(StackActions.replace(routeName, params));
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
};

const popToTop = () => {
    console.log('LOG_popToTop');

    if (isMountedRef.current && navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.dispatch(StackActions.popToTop());
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
};

const popWithStep = (step: any) => {
    console.log('LOG_popwithstep');

    if (isMountedRef.current && navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.dispatch(StackActions.pop(step));
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
};

const reset = (name: string, params?: any) => {
    console.log('LOG_reset', name, params);
    if (isMountedRef.current && navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.dispatch(
            CommonActions.reset({
                routes: [{ name, params }],
            }),
        );
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
};

const goBack = () => {
    navigationRef.current?.goBack();
};

const NavigationService = {
    navigate,
    goBack,
    replace,
    popToTop,
    popWithStep,
    reset
};

export default NavigationService;
